class PublisherJob < ApplicationJob
  queue_as :default

  def perform(message)
    begin
      PublisherService.publish(message)
    rescue StandardError => err
      puts "Error publishing message: #{message}, error: #{err}"
    end
  end

end
