require 'json'

class PublisherService

  def self.publish(message)
    queue = channel.queue('posts_created', durable: true)
    channel.default_exchange.publish(message.to_json, routing_key: queue.name)
  end

  def self.receive
    q = channel.queue('users_deleted', durable: true)
    q.subscribe(block: false) do |delivery_info, properties, payload|
      puts "Mensaje recibido: #{payload}"
      message = JSON.parse(payload)
      if message['action'] == 'delete'
        Post.where(user_id: message['data']['id']).destroy_all
      end
    end
  end

  def self.channel
    @channel ||= connection.create_channel
  end

  def self.connection
    if @connection.nil?
      at_exit do
        puts 'RabbitMQ connection closed'
        @connection.close unless @connection.nil?
      end
      @connection = Bunny.new(
          host: 'localhost',
          port: 5672,
          ssl: false,
          vhost: '/',
          user: 'guest',
          pass: 'guest',
          heartbeat: :server,
          frame_max: 131072,
          auth_mechanism: "PLAIN"
      )
      @connection.start
    end
    @connection
  end

end