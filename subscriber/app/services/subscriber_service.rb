require 'json'


class SubscriberService

  def self.publish(message)
    queue = channel.queue('users_deleted', durable: true)
    channel.default_exchange.publish(message.to_json, routing_key: queue.name)
  end

  def self.receive
      q = channel.queue('posts_created', durable: true)
      q.subscribe(block: false) do |delivery_info, properties, payload|
        puts "Received #{payload}, message properties are #{properties.inspect}"
        message = JSON.parse(payload)
        user_id = message['user_id']
        user = User.find_by_id Integer(user_id)
        unless user.nil?
          user.posts.nil? ? user.posts = 1 : user.posts += 1
          user.save!
        end
      end
  end

  def self.channel
    @channel ||= connection.create_channel
  end

  def self.connection
    if @connection.nil?
      at_exit do
        puts 'RabbitMQ connection closed'
        @connection.close unless connection.nil?
      end
      @connection = Bunny.new(
          host: 'localhost',
          port: 5672,
          ssl: false,
          vhost: '/',
          user: 'guest',
          pass: 'guest',
          heartbeat: :server,
          frame_max: 131072,
          auth_mechanism: "PLAIN"
      )
      @connection.start
    end
    @connection
  end

end