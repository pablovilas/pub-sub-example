class User < ApplicationRecord
  after_destroy :notify_deletion

  def notify_deletion
    SubscriberService.publish({ action: :delete, data: self, timestamp: DateTime.now })
  end
end
